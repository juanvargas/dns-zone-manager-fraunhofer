#! /usr/bin/env python3

from distutils.core import setup

setup(
    name='zonemanager',
    version='0.1',
    description='DNS Zone Manager for GXFS',
    author='Martin Hoffmann',
    author_email='martin@nlnetlabs.nl',
    url='https://www.lightest.eu/',
    packages=['zonedb'],
    scripts=['zonemanager.py'],
    license='MIT',
    install_requires=[
        'falcon==3.1.1',
        'gunicorn==19.9.0',
        'SQLAlchemy==2.0.1'
    ],
    python_requires='>=3.11',
)
