import { useEffect, useState } from 'react'
import Container from 'react-bootstrap/Container'
import AppBar from './components/AppBar'
import ZoneDataView from './views/ZoneDataView'
import axios from 'axios'
import { ConfigContext } from './context/configContext'
import './App.css'

function App() {
  // TODO: Must implement login procedure IF Oauth is chosen.
  const [config, setConfig] = useState({})

  useEffect(() => {
    axios.get('config.json')
    .then((res) => {
      setConfig(res.data)
      console.log("Received axios response: ")
      console.log(res)
    })
  }, [])
  return (
    <>
      <ConfigContext.Provider value={{config: config, setConfig: setConfig}}>
        <AppBar></AppBar>
        <Container style={{ height: "85vh", marginTop:"15vh" }}>
          <ZoneDataView/>
        </Container>
      </ConfigContext.Provider>
    </>
  )
}

export default App