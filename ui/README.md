# React + Vite

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh


# Running:
If running the first time you **must** run `npm install`

Then run `npm run dev` to start the development mode server and page.

# Building:

*If you did not run `npm install` before (e.g. when running in dev mode) you must run now!*

Option 1: Build without docker.
- Run `npm run build` from the ui folder. The build output will be html/css/js files located in the `ui/dist` folder.
- Serve the dist folder with a server of your choice.

Option 2: Build with docker
- run `npm run build` from the ui folder.
- run `docker build -t <your tag> .` from the ui folder.

When building with docker, the output will be a new image based on nginx:latest. This image will be configured to serve the build output files through port 80 with the default nginx configuration.

# Configuration:

Following options show how to modify the UI when running it through the docker image.

**Changing the Logo:**

If you want to modify the look by changing the logo you should mount your image into the docker container. Example:
`docker run --name zone-manager-ui -p 8080:80 -v <local logo path>:/usr/share/nginx/html/logo.png <image-name>`

**Configuring general Configuration:**
Confgurations are loaded from a config.json in the docker image. The default config.json should be overwritten by mounting your desired config:
`docker run --name zone-manager-ui -p 8080:80 -v <local config path>:/usr/share/nginx/html/config.json <image-name>`

## Configuration reference:
Fields you can set in the config.json file:

- backendURL (String) - Tells the UI where to fetch zone data from.
- apiKey (String) - Tells the ui which api key to use to fetch the zone data. **WILL BE DEPRECATED SOON**