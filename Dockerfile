FROM python:3.11

WORKDIR /usr/lib/zonemgr

COPY . /usr/lib/zonemgr

# Install ZM dependencies
RUN pip3 install -e .

# Install dependencies for setup. NSD python bindings (not available in pip)
RUN apt-get update && \
    apt-get install -y nsd && \
    apt-get install -y python3-ldns && \
    apt-get install -y cron && \
    apt-get install -y systemd

RUN update-rc.d nsd defaults && \
    service nsd start 

ENV SQLALCHEMY_WARN_20=0
ENV SQLALCHEMY_SILENCE_UBER_WARNING=1
ENV PYTHONPATH="${PYTHONPATH}:/usr/lib/python3/dist-packages/"

ENV PORT_ENV=16001

EXPOSE "${PORT_ENV:-16001}"
EXPOSE 53 53/udp

CMD ./script.sh $PORT_ENV
