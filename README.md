# DNS ZoneManager for GXFS

The DNS Zone Management is the component responsible for managing the DNS zone file and used for
anchoring the trust framework DID information into the zone file. It is also responsible for resigning
the zone file based on DNSSEC for every new update in the zone file.
The zone manager service was designed to serve the TSPA only.
It manages a zone file by:

- Creating an intermediate database for NS, A, PTR and URI records
- Providing auth mechanism to protect endpoints
- Exposing secure endpoints for managing PTR records (associated with trust frameworks) and URI records (associated with trust lists)
- Updating records and resigning zone file
- Using DNSSEC to provide and additional security layer to the DNS service

The implementation was done based on [LIGHTest zone manager](https://github.com/H2020LIGHTest/ZoneManager).

### Requirements

- OS: Unix based
- Docker v24.0.7
- Domain setup see #Reference (see [config file](config.conf))
- Web server (e.g. nginx)

# Setup

1. Define the environment setup.

In the config.conf file define the primary and secondary servers including DNS names and IP addresses.
The primary server will host and run the zone manager service as well as the zone file.

2. Build and run Docker container
   Build the Docker image

`docker build -t zonemanager .`

3. Run the container

The zone manager will use the port 16001, make sure you map the correct host port and configure
your proxy to direct traffic accordingly.

`docker run -p 16001:16001 zonemanager`

At start up, see the console logs, an API KEY will be generated. For example:

    Generating a ZM API KEY for communication with TSPA, please note it down:
    ===================================================================
    2Vbj1VwrgrLE9aKQjQUMhexiZ51dxSPW
    ===================================================================

This is necessary to make the calls to the endpoints (see [docs](/docs)).
For example, you can check the service status via:

```
curl -X GET \
  -H "Authorization: Bearer 2Vbj1VwrgrLE9aKQjQUMhexiZ51dxSPW" \
  -H "Content-Type: application/json" \
  http://localhost:16001/status
```

# Reference

## System overview

![System overview](docs/system overview.png)

The zone manager is meant to have the TSPA as its only client. The TSPA can perform CRUD operations
in relation to Trust Frameworks and Trust Lists. The Zone Manager takes every operation request and
updates the zone file accordingly. It takes care of adding and deleting records to the zone file and
its required signig.

A simple description of the flow:
At startup, the Zone Manager sets up the DNS server (NSD or KNOT) and creates a data base with the base
SOA, NS and A type resource records wich are then formatted, signed and used to create a zone file.
A Trust Framework creation request can then be initialized by the TSPA. When this happens, the Zone
Manager converts the Trust Framework's URL into a PTR record, stores in the data base and anchors it
in the DNS system by updating the zone file accordingly.
In the case of creation of Trust List DID, the process is similar with only difference being that the
DID are stored as URI type records in the database as well as in the zone file.

This is repeated every time a CRUD operation in relation to Trust Framework URLs and Trust List DIDs.

The current Zone Manager implementation includes a simple UI that allows the administrator to visualize
the zone data. See the [user interface folder](ui)

# Deployment notes

### Zone file persistance

Please note that when the docker container is run, a new zone file gets created. This will be stored inside
the container in the path defined by the environment variable VAR_PATH in the [setup script](script.sh) file
run by docker when the image is built. This zone file is not persisted in the host machine. If you want to provide
persistency for this file please do so by defining volumes in the [Dockerfile](Dockerfile).
